# Barbe lumineuse

Bienvenue sur notre projet de veille technologique.

> ✨ Note: Vous pouvez utiliser le menu de navigation à gauche pour sauter
> rapidement vers une section.

## Présentation du projet

Lors de soirées éclairées, notre client aime bien captiver l’attention des
convives. Se distinguant par sa barbe épaisse et glorieuse, il a eu l’idée
d’aller au-delà des attentes habituelles en nous mandatant pour réaliser son
dernier projet. Ce projet consiste à installer des lumières dans son duvet et de
pouvoir changer les couleurs selon ses désirs et l’ambiance de la soirée. Pour
ce faire, il souhaite pouvoir utiliser une application sur son téléphone qui lui
permettra de combler sa créativité en changeant la façon dont sa barbe
s’illuminera.

Il s’agit donc d’installer un système d’éclairage dans sa barbe et d’en
contrôler les couleurs à partir d’une application.

> ![Exemple de barbe lumineuse](img/resultat-attendu.avif) >
> ![Second exemple de barbe lumineuse](img/resultat-attendu2.avif) <br> Exemple
> du résultat attendu.

Toujours partants pour relever nos manches, la réalisation de ce projet soulève
toutefois plusieurs défis que nous devrons surmonter. Notamment, il faudra
déterminer comment faire tenir les lumières à travers sa pilosité faciale. Il y
a beaucoup de façons possibles pour y arriver, mais il sera nécessaire de
trouver celle qui sera la plus confortable et qui apportera le meilleur
éclairage possible. Il faudra aussi trouver une manière de faire changer
l’agencement des couleurs via son appareil intelligent de façon simple et
efficace. S’ajoutant à la liste des contraintes, il faut s’assurer que le
système soit facilement installable dans la barbe et que les lumières ainsi que
l’application puissent être gérées sans trop de complications. En effet, les
lumières devront pouvoir se tenir en place, durer assez longtemps pour rester
allumées le temps de la soirée de manière stable et elles devront idéalement
résister aux possibles intempéries. Idéalement, la maintenance du système
devrait se faire le plus facilement possible par le client.

### Énumération des contraintes

-   Avoir des lumières dans sa barbe. Le client nous indique qu'il aimerait au
    moins 8 lumières afin que l'effet soit plus convaincant.
-   Idéalement, le client aimerait être en mesure d'afficher plus d'une couleur
    simultanément.
-   Si possible, le client aimerait aussi pouvoir programmer une alternance de
    couleurs ou une oscillation lente entre plusieurs couleurs.
-   Les lumières doivent tenir par elles-mêmes. Le client souhaite éviter
    qu'elles soient accrochées sur ses oreilles, comme ce serait le cas par
    exemple avec un filet.
-   Le système ne doit pas être trop lourd ou trop gros. Il doit bien se
    camoufler dans la barbe.
-   Le système doit être en mesure d'alimenter les lumières pour toute la durée
    d'une soirée, soit un minimum de 3 heures.
-   Le client doit pouvoir reprogrammer l'éclairage à tout moment à partir de
    son téléphone intelligent. Ce téléphone est un iPhone muni d'un port de
    recharge _Lightning_, contrairement aux iPhones plus récents qui utilisent
    désormais un port USB-C.
-   Le système doit être étanche à l'eau autant que possible, puisqu'il sera
    potentiellement exposé à la nourriture et aux breuvages que consomme notre
    client.

### La saga des lumières

La décision la plus importante de tout le projet est sans contredit le choix des
lumières. Il aurait été facile d'utiliser une simple guirlande de Noël pour
ajouter une touche d'éclairage à la toison de notre client. Toutefois, celui-ci
nous a dit qu'il souhaite être en mesure de changer la couleur des lumières.
Malheureusement, la quasi-totalité des guirlandes de Noël sont unicolores ou
bien multicolores, mais ne sont pas configurables. Puisqu'on doit être en mesure
de changer les couleurs à partir d'un téléphone intelligent, nous avons dû
explorer de nombreuses avenues possibles.

> ![Quelques idées qu'on a eu](img/idees-lumieres.avif) <br> Quelques idées
> explorées pour les lumières. À gauche: matrice de DEL adressables
> individuellement. À droite: DEL tricolore individuelle et ruban de DEL
> multicolores.

La première idée que nous avons explorée est le ruban de diodes
électroluminescentes multicolores (voir l'illustration ci-haut). Cette solution
est particulièrement intéressante, puisque cela permettrait au client de
personnaliser la couleur qui est affichée dans sa barbe. Toutefois, ces rubans
ne peuvent afficher qu'une seule couleur à la fois, nous forçant donc à en
utiliser plus d'un. Deuxièmement, ces rubans utilisent pour la plupart un
protocole de communication propriétaire ou encore une application mobile qu'il
nous faudrait déchiffrer par ingénierie inverse.

Nous avons ensuite étudié la possibilité d'utiliser une matrice souple de DEL
adressables individuellement (à gauche dans l'illustration ci-haut). Cette
solution est tentante, puisque chaque DEL (un _pixel_) peut être programmée
individuellement pour être d'une couleur spécifique. Il serait donc possible de
créer des motifs colorés très complexes et d'afficher des dizaines de couleurs
en même temps. Pour diffuser la lumière ainsi produite, nous utiliserions des
fibres optiques afin de la répandre de façon uniforme dans toute la barbe.

Toutefois, un critère important nous empêche de choisir cette solution: notre
système doit être confortable à porter pour de longues périodes de temps, et
avoir un carré dans la barbe ne se prête pas tout à fait au confort. Que nous
reste-t-il donc comme solution alors?

N'ayant pas d'alternative, nous avons commencé à expérimenter avec des DEL
tricolores individuelles (en haut à droite dans l'illustration). Le plan serait
donc d'avoir deux jeux 4 de lumières tricolores branchées en série l'une à la
suite de l'autre. Chacun de ces deux jeux de lumière pourrait être d'une couleur
distincte et nous utiliserions des fibres optiques pour diffuser cette lumière.

C'est alors que Cynthia est arrivée, tel un messie, avec une idée plus que
convenable: plutôt que des matrices souples de DEL, on pourrait utiliser des
anneaux de DEL.

> ![Solution finale pour les lumières](img/idees-lumieres2.avif) <br> À gauche:
> anneau de DEL multicolores adressables individuellement. À droite: exemple de
> fibres optiques et exemple de ceux-ci dans les cheveux de Cynthia.

Ces anneaux, tous comme les matrices carrées, nous permettent de programmer
individuellement chaque lumière pour lui donner une couleur spécifique. Or,
puisqu'ils ont des trous au centre, ces cercles sont plus confortables à porter
et il est facile de les disposer à travers les poils, chose impossible avec les
matrices souples.

Nous avons donc choisi de combiner l'idée des anneaux avec l'idée des fibres
optiques pour donner l'impression que la barbe contient encore plus de lumières
qu'elle n'en contient réellement.

### Le cerveau de l'opération

Initialement, nous avons pensé utiliser un [Arduino Pro Micro] pour contrôler
les lumières, puisque ce microcontrôleur est facilement disponible en ligne ou
en boutique, est abordable et est suffisamment petit pour être camouflé dans une
barbe. Cependant, puisqu’on a statué sur le fait qu’on désire uniquement avoir 2
anneaux de lumières au total, nous n’avons plus besoin des 16 broches _GPIO_ que
le [Arduino Pro Micro] offre. Ainsi, tant qu’à n’utiliser le microcontrôleur
qu’à moitié, nous avons cherché des alternatives plus petites avec moins de
broches _GPIO_.

Nos recherches ont commencé avec le [Seeed Studio XIAO SAMD21]. Cet appareil est
idéal, puisqu’il est relativement abordable (7 $ sur Aliexpress) et sa petite
taille serait plus confortable dans la barbe de notre client que la silhouette
oblongue du [Arduino Pro Micro]. Toutefois, nous avons dû abandonner cette idée
puisque les délais d’expédition auraient potentiellement amené le colis à être
livré _après_ la fin de la session scolaire.

Nous sommes alors tombés sur un autre candidat potentiel : le
[Waveshare RP2040-Zero]. De taille similaire au [Seeed Studio XIAO SAMD21], ce
microcontrôleur est disponible sur Amazon Canada et peut être livré en
48 heures, ce qui est idéal dans notre cas. L’appareil possède 19 broches
_GPIO_, soit encore plus que le Arduino Pro Micro qu’on pensait utiliser au
départ, tout en étant beaucoup plus compact et efficace en énergie. Il peut
aussi être alimenté en électricité par un câble USB-C, qui est plus moderne et
résistant que le port micro-USB qu’on retrouve souvent sur les appareils
Arduino. De plus, il est assez petit pour qu’on puisse le glisser dans une gaine
thermorétractable, le protégeant ainsi de l’humidité et des débris.

Pour toutes ces raisons, nous avons opté pour le [Waveshare RP2040-Zero], qui
semble être le microcontrôleur le plus approprié pour répondre aux besoins de
notre client.

### Une solution électrisante

Puisque le microcontrôleur qu’on a choisi est alimenté par un port USB-C
standard, il n’est pas nécessaire pour nous de créer un circuit électrique pour
contrôler la recharge de la batterie. En effet, tout ce dont nous avons besoin
pour alimenter l’appareil est un chargeur USB. Cela signifie que tout appareil
qui est en mesure de recharger ou d’alimenter un téléphone muni d’un port USB-C
est aussi en mesure d’alimenter le [Waveshare RP2040-Zero]. Ainsi, nous avons
décidé de ne pas nous compliquer inutilement la vie et d’utiliser une banque
d’énergie (_power bank_) conçue pour les téléphones intelligents.

Malheureusement, les banques d’énergie commerciales sont souvent lourdes et
encombrantes, ce qui signifie qu’on ne pourrait les camoufler dans la barbe de
notre client. C’est pourquoi on a décidé de placer la banque dans la poche de
son pantalon et de dissimuler dans son chandail un câble USB-C qui se rend
discrètement jusqu’à sa barbe. Ainsi, notre client bénéficie d’une grande
autonomie de batterie et l’appareil qu’on place dans sa barbe est plus petit et
plus léger.

De plus, puisqu’on propose d’utiliser une batterie externe, il est très facile
pour le client de la remplacer. Il pourrait donc, par exemple, charger deux
banques d’énergie puis en porter une, pour ensuite la débrancher et y substituer
la deuxième lorsque la première est déchargée.

De même, si la batterie commence à faire sentir son âge et n’a plus autant de
capacité qu’au départ, il peut tout simplement acheter une nouvelle batterie
pour la remplacer et il n’aura besoin d’aucune connaissance en soudure ou en
électronique.

### Transmission des données

### Support de communication

Pour être en mesure de contrôler les couleurs des lumières dans la barbe de
notre client, quelques contraintes doivent être respectées. Le client aimerait
être en mesure de contrôler les lumières à partir de son téléphone et il
souhaite pouvoir le faire de n’importe où. Initialement, nous avons envisagé une
application Android, mais notre client possède un iPhone et aucun parmi nous n’a
d’expérience en développement iOS. Nous avons donc pensé à une application web
qui communiquerait à distance avec le microcontrôleur. Toutefois, nous aurions
besoin d’ajouter un module Bluetooth sur notre microcontrôleur et d’employer le
Web Bluetooth API, qui est plutôt difficile d’usage.

Une idée folle est alors survenue dans la tête de Pascal : et si on avait une
application web qui communiquait avec notre appareil par le biais de signaux
audios, par exemple des ultrasons. Nous n’aurions besoin que d’ajouter un
microphone à notre module de contrôle des lumières. Le client approcherait donc
son iPhone de sa barbe et l’application web enverrait les données directement à
travers sa barbe. Ce serait aussi une solution pérenne, puisque, contrairement à
Web Bluetooth API, qui pourrait ou non être déprécié ou retiré des navigateurs
dans un futur rapproché, un signal audio aurait le mérite de ne jamais devenir
obsolète. Tant que le client serait en possession d’un appareil capable de
produire de l’audio, l’application resterait fonctionnelle.

Toutefois, l’idée folle de Pascal n’est pas idéale, puisque n’importe quel son
ambiant serait susceptible de causer de l’interférence ou même de changer les
couleurs des lumières sans le consentement de notre client. De plus, il est
impossible de prévoir comment réagirait un tel système dans un environnement
bruyant, par exemple un _party_ du temps des fêtes.

C’est alors que Wily a proposé une solution innovante : et si on oubliait
entièrement l’idée d’utiliser un hautparleur et un microphone. On pourrait
plutôt brancher _directement_ le téléphone de notre client dans le
microcontrôleur à l’aide d’un câble audio 3,5 mm standard. Nous n’aurions alors
besoin que d’une prise d’écouteurs, ce que le client peut ajouter sans problème
à son iPhone grâce à un adapteur.

### Protocole de transmission

Maintenant que nous avons un moyen de communication pour transmettre les
données, il reste à savoir en quoi exactement consistent ces données.
Initialement, l’idée d’utiliser des ultrasons a été évoquée. Cependant, les
ultrasons pourraient endommager l’ouïe des personnes ou animaux à proximité du
téléphone si ce dernier n’était pas correctement branché au câble audio.

Pascal a donc eu l’idée de « réscusciter » une vieille technologie : le
_Dual-tone multi-frequency signaling_ ([DTMF]). Inventée par Bell vers la fin
des années 1950 pour automatiser la téléphonie terrestre, cette technologie est
mieux connue sous l’appellation _Touch-Tone_. En combinant ensemble deux
fréquences sonores, ce système permet d’encoder et de transmettre des caractères
alphanumériques depuis une ligne terrestre afin qu’ils puissent être reçus et
décodés dans une centrale téléphonique.

> ![Fréquences DTMF](img/dtmf.svg) <br> _Tableau des fréquences DTMF et des
> caractères associés._

Le standard [DTMF] consiste en 8 fréquences qui peuvent être combinées de
16 façons distinctes pour obtenir des caractères spécifiques. Par exemple, pour
obtenir le chiffre `6`, le combiné téléphonique enverrait un signal sonore de
1477 hertz en même temps qu’un signal sonore de 770 hertz. La centrale qui
reçoit l’appel est par la suite en mesure de surveiller ces 16 combinaisons
possibles afin de déterminer quels signaux [DTMF] sont envoyés.

Ce protocole de communication s’avère particulièrement utile pour notre projet,
puisqu’il est audible à l’oreille nue et a prouvé sa robustesse et sa fiabilité
au fil des 7 dernières décennies. En effet, même les équipements téléphoniques
en très mauvais état arrivent sans problème à relayer des signaux [DTMF], peu
importe la qualité d’appel. Ce système a été conçu dès le départ pour pallier
aux interférences et éviter les faux positifs. Ainsi, c’est le protocole idéal
pour échanger des données entre notre application mobile et l’appareil qui
contrôle les lumières.

### Le panneau de contrôle

Pour contrôler les lumières, nous envisageons de créer une application mobile
qui communiquera avec le microcontrôleur en utilisant des signaux [DTMF]. Nous
avons donc besoin d'un mécanisme pour générer ces signaux à partir du navigateur
web du client. Pour y parvenir, nous utiliserons le [Web Audio API], une
interface riche en fonctionnalités pour manipuler et synthétiser des sons dans
les applications web. Nous allons exploiter cette API pour produire
simultanément les deux tonalités requises par le standard [DTMF] pour
transmettre l'un des 16 caractères possibles. En utilisant le Web Audio API,
nous pouvons garantir une reproduction sonore de haute précision et assurer
ainsi une communication efficace entre l'application web et le microcontrôleur.

Puisque chaque DEL des anneaux lumineux peut être adressée individuellement,
nous enverrons chaque couleur pour chaque DEL l'une après l'autre. Puisque le
système [DTMF] supporte 16 caractères distincts, nous pouvons décider de façon
tout à fait arbitraire que le caractère `*` veut plutôt dire `E` et que le
caractère `#` veut plutôt dire `F`. Ainsi, nous serions en mesure de générer les
caractères suivants: `0`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`, `A`, `B`,
`C`, `D`, `E`, `F`. Si cette séquence vous semble familière, c'est parce qu'elle
correspond aux 16 caractères du [système hexadécimal]. Assumant que nous
voulions permettre de choisir parmi 256 couleurs distinctes, nous n'aurions
besoin que de deux caractères pour transmettre cette couleur
(<math><mrow><msub><mo>log</mo><mn>16</mn></msub><mo>(</mo><mn>256</mn><mo>)</mo>
<mo>=</mo><mn>2</mn></mrow></math>&nbsp;). Donc, si par exemple nous avions 3
DEL à programmer, nous pourrions envoyer la séquence `F3197D` pour donner la
couleur `243` à la première DEL, `19` à la seconde et `125` à la troisième.

Assumant que nous avons 2 anneaux de 16 DEL, nous devrions envoyer 64 caractères
[DTMF] pour programmer chaque DEL. Nous n'avons pas encore fait de test à cet
égard, mais assumant qu'on puisse envoyer les caractères à un débit de 0,1
hertz, en multipliant ce nombre par 64, on obtient 6,4 hertz. Puisque chaque
hertz équivaut à une seconde, notre application web serait en mesure de
programmer toutes les DEL en moins de 7 secondes.

Nous utilisons également l'API [LocalStorage] pour enregistrer les derniers
paramètres du client dans son navigateur afin qu'il soit en mesure de modifier
rapidement les couleurs à sa guise.

## Notre proposition

Après de longues réflexions et expérimentations avec les diverses technologies
étudiées, nous avons statué pour
[utiliser un microcontrôleur](#le-cerveau-de-l39opération) pour contrôler
[des lumières à DEL](#la-saga-des-lumières). Ce microcontrôleur sera lui-même
programmé par le téléphone intelligent de notre client en utilisant
[une application web](#le-panneau-de-contrôle). Le tout sera alimenté
[par une banque d'énergie](#une-solution-électrisante) et utilisera un ingénieux
[système de communication audio](#transmission-des-données) pour faciliter
l'échange des données.

### Schématisation matérielle

> ![Diagramme matériel du projet](img/diagramme-materiel.svg) <br> Diagramme
> matériel du projet.

Sur le plan matériel, nous utiliserons un câble USB-C pour relier notre
[banque d'énergie](#une-solution-électrisante) à notre
[microcontrôleur arduino](#le-cerveau-de-l39opération). Ce microcontrôleur sera
ensuite relié à deux [anneaux de lumières](#la-saga-des-lumières) par des câbles
d'alimentation ainsi qu'un câble de communication utilisant le protocole
[WS2812]. Pour relier le téléphone intelligent de note client au
microcontrôleur, nous utiliserons d'abord un adapteur Lightning permettant
d'ajouter un port audio 3.5mm à son iPhone, puis nous relierons ce port 3.5mm au
microcontrôleur à l'aide d'un câble audio. Chaque DEL des anneaux de lumières
sera ornée d'une grappe de fibres optiques intégrée de manière discrète pour
permettre une répartition uniforme de la lumière tout en offrant un aspect
esthétique attrayant. Ces anneaux de lumières seront tenus en place par des
peignes ou des pinces à cheveux.

La batterie sera placée dans les poches du client afin de ne pas encombrer sa
toison. Le câble audio qui relie le microcontrôleur au iPhone du client suivra
le même chemin que le câble USB-C de la batterie et partira donc de sa barbe
pour se rendre à ses poches, où il sera branché dans l'adapteur Lightning de son
téléphone. Ce câble peut demeurer branché en permanence, mais ce n'est pas
nécessaire et il peut être débranché sans souci après la programmation du
microcontrôleur.

### Schématisation logicielle

> ![Diagramme logiciel du projet](img/schema-logiciel.svg) <br> Diagramme
> logiciel du projet.

Sur le plan logiciel, notre client pourra utiliser
[l'application web](#le-panneau-de-contrôle) pour sélectionner les couleurs
qu'il souhaite afficher dans sa barbe. Il y trouvera une représentation visuelle
des anneaux de lumières ainsi qu'un aperçu du résultat prévu. L'application
enregistrera alors sa sélection au moyen de l'API [LocalStorage] et lui
demandera ensuite de brancher le câble audio dans l'adapteur Lightning de son
iPhone. Lorsque c'est fait, il transmettra les couleurs pour chacune des DEL
[au moyen de signaux DTMF](#transmission-des-données).

Lorsque le [microcontrôleur arduino](#le-cerveau-de-lopération) reçoit des
signaux audios, il emploie la bibliothèque [Goertzel library for Arduino] afin
de les décomposer en caractères DTMF. Ensuite, lorsqu'il a reçoit les couleurs
de chaque DEL des deux anneaux, il prépare son envoi aux anneaux de lumières.

[Ces anneaux de lumières](#la-saga-des-lumières) utilisent le protocole [WS2812]
pour régler les couleurs. Ce protocole propriétaire s'apparente au protocole
[RS-232] et est quelque peu compliqué à travailler avec. Plutôt que de nous
tirer les cheveux et réinventer la roue, nous utiliserons la bibliothèque
[Adafruit NeoPixel] pour effectuer pour nous la communication via [WS2812] et
envoyer les instructions aux anneaux de lumières.

### Réponse aux contraintes

#### 8 lumières minimum

Les deux anneaux de lumières ont chacuns 16 DEL, ce qui porte le nombre total de
lumières à 32.

#### 2 couleurs minimum

Chaque DEL peut afficher une couleur distincte. Il sera donc possible d'afficher
jusqu'à 32 couleurs en même temps.

#### Alternance de couleurs

Créer une alternance de couleurs ou une progression lente de couleurs n'est pas
prévue dans la version initiale du produit, mais il nous sera possible d'ajouter
cette fonctionnalité dans le futur.

#### Rien d'accroché aux oreilles

Les lumières seront retenues dans la barbe avec des peignes ou pinces à cheveux,
évitant ainsi de les accrocher aux oreilles.

#### Petit et léger

En déplaçant la batterie dans la poche de pantalon du client, nous évitons
d'alourdir ou d'encombrer sa barbe. Cela nous permet aussi de grandement
augmenter la capacité totale de la batterie.

#### Doit durer le temps d'une soirée

Puisque la batterie peut être échangée avec une autre batterie en moins de 10
secondes, le système peut rester alimenté aussi longtemps que le client le
désire.

#### Doit être compatible avec un iPhone

Puisque nous utilisons des signaux audio pour transmettre les commandes, notre
système est compatible avec des appareils iPhone ayant un port audio, iPhone
ayant un port Lightning, iPhone ayant un port USB-C, Android ayant un port
audio, Android ayant un port USB-C, ou n'importe quel autre appareil ayant un
port audio 3.5mm. Le système est donc compatible avec les besoins actuels du
client ainsi qu'avec tout besoin futur qu'il pourrait avoir.

#### Résistance à l'eau

Les anneaux de lumières sont résistantes à l'humidité et le microcontrôleur sera
recouvert d'une gaine thermorétractable étanche pour la protéger des liquides.

## Mise en oeuvre

Au niveau des matériels que nous allons utiliser, il y a beaucoup de trucs qui
seront utiles afin de gérer à la fois l’esthétique de la barbe ainsi que le bon
fonctionnement des lumières dans celle-ci. Voici plus en détail les trucs que
nous avons utilisés.

### Le chemin des données

Tel que mentionné précédemment, le cœur du projet sera réalisé à l’aide du
[Waveshare RP2040-Zero]. Ce microprocesseur, fabriqué par Raspberry Pi
Foundation, va servir de petit ordinateur afin de pouvoir traiter les demandes
que l’utilisateur va exécuter. Le fait qu’il a une petite taille va permettre de
le cacher discrètement derrière la barbe. En plus, son niveau de performance est
assez efficace pour fournir aux demandes voulues.

> ![Rasperry Pi Waveshare RP2040-Zero](img/RP2040-Zero.jpg) <br> Waveshare
> RP2040-Zero.

Pour offrir de l’énergie au système afin que les lumières puissent bien
s’allumer, nous allons utiliser une banque d’alimentation USB-C de 30,000 mAh.
Le fait qu’on utilise un fil USB-C va rendre le tout simple à utiliser, car il
est universel et il est facilement remplaçable. La batterie va aussi bien se
mettre dans la poche de l’utilisateur. N’importe quel modèle qui correspond à ce
qui est mentionné peut fonctionner pour le projet. Dans notre cas, nous allons
utiliser la batterie externe de la marque YYDS qui est facilement trouvable sur
Amazon ou d’autres sites d’achats.

Ensuite, les informations seront transférées à l’aide d’un câble bornier à vis.
Dans notre cas, nous allons utiliser un [adapteur audio stéréo mâle vers 3
broches femelles]. Il est de la marque zdyCGTime qui fournit la majorité de ses
produits en ligne. Avec ce câble, le système va ainsi pouvoir bien transférer
aux lumières les informations sonores qu’il va recevoir tout en n’étant pas trop
encombrant pour la barbe.

### Conception de la barbe lumineuse

Pour ce qui est des lumières, des [anneaux lumineux] avec 8 DEL vont être
accrochés derrière la barbe. Plus précisément, nous allons utiliser le modèle
WS2812B de la marque Steamedu. Un des avantages de ce produit est qu’elle est
bien compatible avec les items conçus par Raspberry Pi. Avec son diamètre de
25mm, il sera possible de mettre 2 anneaux derrière la barbe sans que cela
prenne trop d’espace. Sur ces cerceaux lumineux, nous allons utiliser des
lanternes décoratives qui vont rajouter des éclats de lumières dans la barbe
afin d’offrir un magnifique design à travers la pilosité faciale. Elles sont
faites en fibres optiques.

En tant que tel, nous allons utiliser plusieurs produits artisanaux afin de bien
maintenir le système en place. Notamment, nous allons d’abord créer un moule
initial à l’aide d’une [pâte à modeler]. Avec un peu de farine, cela va faire en
sorte qu’elle ne colle pas trop en général. Cette pâte reste facile à
transporter puisqu’elle est solide. À l’aide de ce moule qui va suivre la forme
de l’anneau lumineux, il y aura un mélange de silicone à l’intérieur de celui-ci
afin de créer une forme réaliste et semblable à l’anneau de lumière.

Le silicone va prendre entre 6 et 12 heures pour sécher et prendre la forme
solide du moule. Puisqu’il est liquide, il faudra faire attention à ce qu’il ne
touche pas les lumières afin de ne pas les endommager. Cela dit, une fois bien
durci, il va bien conserver la forme qu’il a forgé dans son moule tout en
gardant une souplesse qui le rendra malléable.

Pour l’époxy, dans le même principe que le silicone, il va aussi prendre entre 6
et 12 heures pour sécher et son état liquide fait en sorte qu’il ne devra pas
toucher les lumières. L’époxy sera utilisé pour faire un moule autour du modèle
en silicone. En effet, grâce à sa propriété antiadhésive, l’époxy ne va pas se
coller avec la maquette en silicone et va former une forme que l’on pourra
mettre autour de l’anneau de lumière une fois qu’il sera solidifié. Le résultat
sera translucide et semi-rigide.

Un autre moule que nous utilisons est fait en thermoplastique, puisqu’il est
malléable tout en restant simple à utiliser. Contrairement au silicone et à
l’époxy, il peut être directement utilisé sur l’anneau de lumières, mais il va
coller sur les fils. Une fois le modèle conçu, un conseil que l’on peut vous
offrir est de ne pas refaire un moule en thermoplastique par-dessus puisque les
2 moules vont fusionner ensemble. Bref, grâce à ce bricolage, cela va tenir en
place les fibres optiques avec son anneau de lumières à DEL.

## Expérimentations

Afin de concrétiser le projet, nous devons maintenant fabriquer le système
permettant d’arriver à la magnifique barbe étincelante que le client désire.
Pour arriver au produit final, nous allons ainsi passer vers une série
d’expérimentation. Que cela soit simplement pour l’esthétique du dispositif ou
bien pour la série de lumière, plusieurs tests seront effectués afin d'obtenir
le résultat souhaité.

### Porte-fibres optiques

Notre première idée est de créer un porte-fibres optiques qui s’installera sur
l'anneau de lumières sans colle et sans abimer les lumières. Ainsi, si une
lumière brise, les fibres optiques pourront être utilisées sur un nouvel anneau
de lumières. Pour parvenir à faire un porte-fibres qui s’installe bien sur
l’anneau, nous avons décidé de créer des moules de ce dernier afin de pouvoir
les utiliser avec divers matériaux sans abimer les lumières.

### Fabrication des moules

#### 1. Moule en silicone

-   1.1. Prise d’empreinte de la lumière dans une pâte à modeler.
-   1.2. Remplir l’empreinte de silicone liquide.
-   1.3. Résultat une fois le silicone figé. Résultat souple.

> ![Étapes moule en silicone](img/moule-silicone.jpg) <br> Étapes de la création
> du moule en silicone.

#### 2. Moule en thermoplastique

-   2.1. À partir du moule en époxy:
-   2.2. Remplir le moule de thermoplastique (voir la vidéo ci-dessous).
-   2.3. Résultat une fois le thermoplastique figé. Résultat rigide.

> ![Étapes moule en thermoplastique](img/moule-thermoplastique.jpg) <br> Étapes
> de la création du moule en thermoplastique.

> <iframe style="width: 100%" height="500" src="https://www.youtube.com/embed/LkcPdLwcVCA" title="thermoplastique" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
> <br> Vidéo démontrant l'étape 2.2.

#### 3. Porte-fibres optiques en époxy

-   3.1 Dans un moule creux en forme de beigne, nous avons versé le mélange
    d’époxy liquide sur lequel nous avons déposé le moule de silicone en forme
    de l’anneau de lumières.

-   3.2 – 3.3 Une fois l’époxy figé, nous avons percé vis-à-vis les DEL et coupé
    vis-à-vis les fils pour que le porte-fibres s’adapte bien et pour permettre
    d’insérer les fibres optiques.

-   3.4 – 3.5 Insertion des fibres optiques dans les trous vis-à-vis des DEL.

> ![Étapes portes-fibres optiques en epoxy](img/porte-fibres-epoxy.jpg) <br>
> Étapes de la création du porte-fibres optiques en epoxy.

#### 4. Porte-fibres optiques en pâte à modeler

-   4.1 À partir du moule rigide en thermoplastique, on a moulé un porte-fibre
    (voir la vidéo ci-dessous)
-   Les autres étapes seront similaires aux étapes 3.2 et 3.4.

> <iframe style="width: 100%" height="500" src="https://www.youtube.com/embed/SfrQvwqDrXM" title="pâte à modeler" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
> <br> Vidéo démontrant l'étape 4.1.

#### Support dans la barbe

Les fibres optiques offrent déjà un bon support, un peu comme un peigne dans une
chevelure. Par contre, il faut prévoir un moyen de solidifier le tout.

Essai 1 : À partir d’une pince insérée dans le centre de l’anneau, on espérait
« pincer » une portion de la barbe. Par contre, comme cette dernière est déjà
parsemée de fibre optique, cela n’apportait pas la solidité souhaitée. De plus,
l’anneau de lumière risquait de se détacher du porte-fibre, ce qui réduirait la
luminosité via les fibres.

> ![Pince pour le support de la barbe](img/support-barbe1.jpg) >
> ![Pince pour le support de la barbe](img/support-barbe2.jpg) <br> Étapes du
> premier essai.

Essai 2 : L’ajout d’un élastique qui entoure le porte-fibre et l’anneau permet
de garder le porte-fibre et l’anneau ensemble et d’y insérer la pince. Ainsi, la
pince a accès à plus de barbe à pincer, offrant ainsi un meilleur soutien, tout
en étant discret puisque c’est à l’arrière.

> ![Pince pour le support de la barbe](img/support-barbe3.jpg) >
> ![Pince pour le support de la barbe](img/support-barbe4.jpg) >
> ![Pince pour le support de la barbe](img/support-barbe5.jpg) <br> Étapes du
> deuxième essai.

### Contrôle des lumières

Pour être en mesure de contrôler les anneaux de lumières à partir de notre
microcontrôleur, nous avons d'abord exploré la bibliothèque [FastLED], connue
pour sa flexibilité et sa puissance. Cependant, nous avons rencontré des
difficultés lors de son intégration avec le Waveshare RP2040-Zero. Face à ces
défis, nous avons opté pour la bibliothèque [Adafruit NeoPixel]. Cette
transition s'est avérée fructueuse. Avec Adafruit NeoPixel, nous avons pu faire
fonctionner les anneaux de DEL de manière fluide et précise. Contrôler les
couleurs et expérimenter avec différentes séquences lumineuses a été aisé. Cette
expérimentation nous a permis de mieux comprendre les nuances du contrôle des
DEL et de choisir la solution la plus adaptée à notre projet.

> ![Exemple du code arduino](img/TestBibliotheque.png) <br> Bloc de code pour un
> jeu de 10 couleurs.

### Communication DTMF

Pour échanger les données entre le téléphone de notre client et le
microcontrôleur, nous avons opté pour le protocole [DTMF]. Ainsi, il nous faut
un moyen de générer et transmettre ces signaux sur le téléphone et un moyen de
recevoir et de décoder ces signaux sur le microcontrôleur.

#### BarbeCtl

Nous avons d'abord commencé par la portion décodage des signaux DTMF. Pour ce
faire, nous avons créé un programme simple nommé [BarbeCtl] qui utilise la
bibliothèque [Goertzel library for Arduino]. Ce programme est capable de décoder
les signaux [DTMF] en écoutant les fréquences sonores reçues via le câble audio.

Pour tester le programme, il nous faut préparer le microcontrôleur afin qu'il
soit en mesure de recevoir de l'audio par ses broches GPIO. Malheureusement,
personne sur les internets ne semble être du même avis sur la bonne marche à
suivre pour connecter un jack audio à un microcontrôleur. Nous avons donc
consulté de [nombreuses] [sources] [conflictuelles] avant de finalement statuer
sur une configuration qui nous semblait raisonnable trouvée dans [un fil de
discussion] sur Electrical Engineering Stack Exchange.

> ![circuit électronique pour BarbeCtl](img/circuit-audio.avif) Circuit
> électronique que nous avons choisi pour lire l'audio avec un Arduino.

Au départ, tout semblait bien fonctionner et Pascal arrivait à lire des signaux
[DTMF] sans problème. Toutefois, lorsqu'il a tenté de reproduire son succès
devant les autres membres de l'équipe, le programme ne fonctionnait plus. On a
donc testé et re-testé toutes les composantes électronique du système, traçant
chaque connexion, vérifiant chaque résistance, mesurant chaque tension. Au bout
de plusieurs heures de tests, l'équipe est repartie bredouille, sans avoir
trouvé la cause du problème.

> ![test de BarbeCtl](img/dtmf-arduino.avif) Tests infructueux de [BarbeCtl] en
> classe.

Or, lors de la prochaine rencontre de l'équipe, Pascal a décidé de refaire un
test et, à sa surprise, tout fonctionnait à nouveau. Après un éclat de rire
général, nous avons réalisé que le problème était tout simplement dû au volume
audio de l'ordinateur de Pascal, qui était trop bas pour que le microcontrôleur
puisse décoder les signaux. En augmentant le volume, tout fonctionnait à
nouveau. Lors de ses tests à la maison, Pascal avait mis le volume au maximum,
mais en classe, son volume était à environ 60%.

Plusieurs facteurs peuvent influencer la qualité de la lecture audio par le
microcontrôleur. Par exemple, le volume de l'audio, le degré de sensibilité de
capture et la durée des signaux DTMF.

#### BarbeGen

Pour tester ces trois facteurs, nous avons créé une application web nommée
[BarbeGen]. Cette application permet de générer des signaux DTMF et d'en ajuster
le volume et la durée. Ainsi, nous avons débuté une batterie de tests afin de
découvrir les paramètres optimaux pour la lecture des signaux DTMF par le
microcontrôleur.

| volume  | durée      | sensibilité | résultat                                         |
| ------- | ---------- | ----------- | ------------------------------------------------ |
| 100%    | 500 Hz     | 2000 Ω      | ✅ fonctionne                                    |
| 70%     | 500 Hz     | 2000 Ω      | ❌ non détecté                                   |
| 80%     | 500 Hz     | 2000 Ω      | ✅ fonctionne                                    |
| 70%     | 500 Hz     | 500 Ω       | ✅ fonctionne                                    |
| 60%     | 500 Hz     | 500 Ω       | ⚠️ fonctionne quelques fois                      |
| **60%** | 500 Hz     | **1000 Ω**  | ✅ fonctionne                                    |
| 60%     | 250 Hz     | 1000 Ω      | ✅ fonctionne                                    |
| 60%     | 100 Hz     | 1000 Ω      | ⚠️ signal corrompu: caractères fantômes détectés |
| 60%     | **200 Hz** | 1000 Ω      | ✅ fonctionne                                    |

D'après ces tests, nous avons déterminé que la durée optimale est de 200 Hz,
soit 200 ms et la sensibilité de capture optimale est de 1000 Ω. Dans cette
configuration, la lecture fonctionne bien même avec un volume audio de 60%.
Ainsi, nous n'avons qu'à dire à notre client de monter son volume au maximum et
la transmission devrait bien s'effectuer, même si son volume maximum est de 40%
plus faible que le volume maximum de l'ordinateur de Pascal.

#### Avis de l'équipe

Le système de communication DTMF semble bien fonctionner et est résistant aux
interférences lorsque bien configuré. Le câble audio bornier à vis nous permet
de rapidement prototyper et tester notre système, mais la borne à vis est somme
toute très grosse pour un système qui se veut petit et discret. Nous allons donc
probablement devoir couper la borne à vis et souder directement les fils sur le
microcontrôleur.

Pour faire les tests, nous avons utilisé le [Arduino UNO R3] afin de ne pas
risquer de briser notre microcontrôleur [Waveshare RP2040-Zero]. Le UNO R3 est
facile à configurer et à programmer, mais il est beaucoup plus gros que le
RP2040-Zero que nous souhaitons utiliser au final. Normalement, tout le code qui
est compatible avec le UNO R3 devrait aussi être compatible avec le RP2040-Zero,
mais nous allons attendre le prochain TP pour utiliser le RP2040-Zero, puisqu'il
doit être soudé, ce qui est moins pratique.

La bibliothèque [Goertzel library for Arduino] que nous avons évalué est très
simple à utiliser et semble bien fonctionner. Nous allons donc conserver cette
solution pour la suite du projet.

## Montage du prototype

### Matériel nécessaire

-   1x porte-fibres optiques (voir la section _Fabrication des moules_ plus
    haut)
    -   [kit de fabrication de moules en silicone]
    -   [perles thermoplastiques]
    -   [kit de résine époxy transparente]
    -   [pâte à modeler]
-   1x [Arduino UNO R3]
-   1x câble sacrificiel avec au moins 3 fils à l'intérieur
    > nous avons utilisé un câble réseau catégorie 5e, mais tout type de câble
    > devrait suffire
-   2x résistances de 100 kΩ
-   1x condensateur de 10 µF
-   1x câble audio 3.5mm
-   1x jack audio 3.5mm
-   1x fer à souder
-   fil à souder **sans plomb**
    > sérieusement, pourquoi est-ce qu'on vend encore en 2024 du fil à souder
    > contenant des substances neurotoxiques?!
-   1x pistolet à air chaud _ou_ un briquet à long embout (style barbecue)
-   6x fils de prototypage pour planche _breadboard_
-   1x [anneau lumineu]
-   1x multimètre

### Création du circuit électronique pour l'audio

1. Référez-vous au circuit électronique de la section intitulée
   [Barbectl](#/?id=barbectl). Vous vous baserez sur ce circuit tout au long de
   la période de soudure.
2. Chauffez le fer à souder. Il doit être assez chaud pour faire fondre le fil à
   souder, mais sans plus.
3. Soudez le condensateur à un fil de prototypage.
4. Soudez à l'autre broche du condensateur une des résistances de 100 kΩ, puis
   soudez ensuite la deuxième à cette même broche de telle sorte que vous ayez
   une structure de la sorte:

```mermaid
flowchart LR
    F1[fil] --> C(condensateur)
    C --> R1([résistance])
    C --> R2([résistance])
```

5. Soudez ensuite un fil de prototypage à l'intersection du condensateur et des
   deux résistances:

```mermaid
flowchart LR
    F1[fil] --> C(condensateur)
    C --> R1([résistance])
    C --> F2[fil]
    C --> R2([résistance])
```

6. Ajoutez un fil de prototypage à l'extrémité des deux résistances:

```
flowchart LR
    F1[fil] --> C(condensateur)
    C --> R1([résistance]) --> F4[fil]
    C --> F2[fil]
    C --> R2([résistance]) --> F5[fil]
```

7. Ajoutez un autre fil à l'une des deux résistances:

```mermaid
flowchart LR
    F1[fil A] --> C(condensateur)
    C --> R1([résistance]) --> F4[fil B]
    C --> F2[fil D]
    C --> R2([résistance]) --> F5[fil C]
    F6[fil E] --> R1
```

8. Prenez ensuite votre fil du jack audio 3.5mm (le réceptacle ou l'emboût dit
   "femelle") et insérez-y le câble audio 3.5mm (l'emboût dit "mâle"). Nous
   insérons ainsi le câble dans le jack afin de nous rappeler qu'il ne faut pas
   couper ce côté du jack. Prenez-le donc par l'autre extrémité (celle avec les
   terminaux à vis) et coupez entièrement les terminaux à vis.

9. Il est maintenant temps de relier le jack au circuit que vous avez soudé,
   mais avant, il faut déterminer quel fil correspond à au côté droit, au côté
   gauche et au _ground_ (mise à la terre). Pour ce faire, on utilise un
   multimètre.

    Mettez le multimètre en mode diode (ou "continuité") et placez une sonde sur
    l'extrémité (la pointe) de l'emboût mâle du câble audio 3.5mm. Ensuite,
    utilisez la seconde sonde pour tester les fils du câble audio 3.5mm. Lorsque
    vous constatez la continuité de la connexion, vous avez trouvé le fil
    correspondant à la pointe de l'emboût mâle. Répétez l'opération pour le
    milieu du jack (entre les deux anneaux) et pour la base du jack (le
    _ground_).

    La pointe correspond toujours à l'audio de gauche, le centre correspond à
    l'audio de droite, et la base correspond à la mise à la terre.

    Dans notre cas, le fil de la pointe est de couleur blanc, le fil du centre
    est de couleur rouge et le fil de la base est de couleur noir, mais ces
    résultats peuvent varier, alors il faut toujours tester avec un multimètre.

10. Soudez le fil de l'audio de gauche (la pointe) et de l'audio de droite (le
    centre) au fil menant au condensateur et soudez le fil de la mise à la terre
    (la base du jack) au fil qui mène à la résistance:

```mermaid
flowchart LR
    F1[fil A] --> C(condensateur)
    C --> R1([résistance]) --> F4[fil B]
    C --> F2[fil D]
    C --> R2([résistance]) --> F5[fil C]
    F6[fil E] --> R1
    F7[Audio L+R] --> F1
    F8[Audio GND] --> F6
```

11. Glissez le tout dans une gaine thermoretractable et utilisez le pistolet à
    air chaud pour la rétracter. Si vous n'avez pas de pistolet à air chaud,
    vous pouvez utiliser un briquet à long embout (style barbecue) pour
    rétracter la gaine. Attention de ne pas brûler la gaine.

> ![Assemblage du circuit audio](img/assemblage-fil-audio.avif) <br> Assemblage
> du circuit audio.

12. Félicitations, vous avez maintenant un circuit de lecture audio pour votre
    microcontrôleur.

### Création du câble d'extension pour l'anneau de lumière

1. Prenez un câble sacrificiel avec au moins 3 fils à l'intérieur et coupez-le à
   la longueur désirée. Ce câble devrait être assez long pour relier votre barbe
   à votre poche de pantalon ou un sac à dos.

2. Coupez les deux embouts du câble sactificiel et dégainez les fils.

> ![Câble sacrificiel](img/prep-cable.avif) <br> Câble sacrificiel.

3. Retirez tous les fils qui ne sont pas nécessaires. Vous devriez conserver un
   fil pour l'alimentation 5v, un fil pour la mise à la terre et un fil pour la
   communication de données.

4. Coupez le connecteur de plastique d'un des deux fils de l'anneau de lumière
   et dégainez les fils. Connectez ces fils aux fils du câble sacrificiel,
   soudez-les ensemble et refermez avec une gaine thermoretractable.

> ![Câble d'extension](img/cable-extension.avif) <br> Liaison des deux câbles.

5. Dégainez l'autre extrémité du câble sacrificiel et ajoutez-y des fils de
   prototypage et refermez avec une gaine thermoretractable.

6. Félicitations, vous avez maintenant un câble d'extension pour votre anneau de
   lumière.

### Assemblage des diverses parties

1. Préparer le moule qui va servir de support à la lumière. Vous pouvez
   consulter les étapes de la réalisation du moule dans la section
   “Expérimentations”.

2. Insérer le support à fibres optiques dans l’anneau de lumière. Assurez-vous
   que les trous dans le support sont bien alignés avec les lumières DEL.

3. Placer les fibres de lumière à l’intérieur des trous dans le support.

4. Mettre la pince sous l’élastique qui se situe en dessous de l’anneau
   lumineux. L'ensemble devrait ressembler à ceci:

    > ![Support de l'anneau DEL](img/setup.png) <br> Support qui tient l'anneau
    > de lumière.

5. Une fois le montage de lumière terminé, vous pouvez connecter l’ensemble
   lumineux avec le microcontrôleur. Assurez-vous que les connexions sont bien
   connectées afin que les informations soient bien transmises.

    Le fil de l’alimentation 5v doit être connecté à la broche 5v du
    microcontrôleur. Le fil de la mise à la terre doit être connecté à la broche
    GND du microcontrôleur. Le fil de la communication de données doit être
    connecté à la broche 11 du microcontrôleur (modifiez la variable
    `GPIO_PIN_ANNEAU` dans le code Arduino si vous utilisez une autre broche).

    Pour le fil audio, branchez le fil d'alimentation dans le 5v, le fil de mise
    à la terre dans le GND et le fil de communication dans la broche A0 du
    microcontrôleur (modifiez la variable `GPIO_PIN_AUDIO_L` dans le code
    Arduino si vous utilisez une autre broche).

6. Connecter la batterie avec le microcontrôleur. Assurez-vous que les
   connexions sont bien sécurisées afin d’éviter les petits pépins.

7. Branchez la prise audio de votre téléphone au circuit audio du
   microcontrôleur.

8. Programmer l’Arduino avec l’application Arduino IDE sur votre ordinateur.
   Vous pouvez télécharger le code fourni à l’aide de BarbeCtl qui se situe dans
   le dépôt git.

9. Utilisez le site web qui vous est offert dans le dépôt git BarbeGen qui va
   vous permettre de choisir les opérations que vous désirez à l’aide d’un
   signal audio. Sur cette page, vous pouvez notamment choisir les couleurs à
   afficher, les animations que vous voulez ainsi que le rythme qu’elle va
   suivre. Une version hébergée de BarbeGen est aussi disponible sur
   https://app.barbe.dad

10. Amusez-vous lors de votre évènement avec votre barbe ainsi que le système de
    lumière. N’oubliez pas de partager votre plaisir avec vos amis afin de
    maximiser l’expérience.

<iframe width="928" height="500" src="https://www.youtube.com/embed/0XzQhs7k4NA" title="lumieres" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
<br> Vidéo démontrant ce que l'on peut accomplir avec les lumières.

## Logiciel du microcontrôleur

Nous utilisons la bibliothèque [Goertzel library for Arduino] pour décoder les
signaux DTMF, la bibliothèque [Adafruit NeoPixel] pour contrôler les lumières et
finalement nous utilisons aussi [TaskScheduler] pour permettre à ces deux
systèmes distincts d'opérer en même temps sur l'unique fil d'exécution du
microcontrôleur.

### Site web

D’abord, le client a accès à une page web qui lui permet de choisir les couleurs
qui seront affichées dans les lumières. Sur celle-ci, l’utilisateur pourra
programmer à volonté l’affichage de sa jolie barbe. Il pourra aussi utiliser des
affichages prédéfinis et les sauvegarder. Une fois qu’il aura pris sa décision,
cela va envoyer des signaux [DTMF] qui vont être utilisés pour envoyer les
données par l’audio. Le site web sera conçu à l’aide de [Svelte]. À titre
informatif, c’est une structure JavaScript qui est utilisée afin de créer des
applications web. À la différence de certaines structures, elle compile le
script directement lors de la création de l’application. Nous avons opté pour
ceci puisque cela nous offre une souplesse au niveau de la création de la page
tout en optimisant les performances de l’application. Cela va aussi offrir pour
l’utilisateur un affichage réactif qui va améliorer son expérience lors de
l’utilisation puisque cela peut adapter les données en temps réel.

> ![Site web BarbeGen](img/website.png) <br> Aperçu de BarbeGen, le site web
> permettant de gérer les lumières.

### Arduino

Ensuite, vu que la gestion des lumières est gérée à l’aide d’un microprocesseur
Arduino, nous travaillons avec leur application fournie, c’est-à-dire Arduino
IDE. Ce logiciel travaille avec le langage C++ et fournit les bibliothèques
nécessaires afin de bien réaliser le projet. Puisque cette application est très
compatible avec notre microprocesseur, cela va faciliter la réalisation du
programme. Ainsi, les informations que le site web va envoyer à l’aide d’un
signal DTMF vont être bien assimilées. Le script va lire en continu les
informations qu’il va recevoir et va afficher, dans les lumières, les bonnes
couleurs ainsi que les bonnes animations.

## Annexe

### Matériel expérimenté

-   [Arduino UNO R3]
-   planche de prototypage (_breadboard_)
-   multimètre
-   pinces alligator
-   câbles de prototypage
-   résistances 100 kΩ
-   condensateur 10 µF
-   diodes simples
-   diodes électroluminescentes (DEL) RGB
-   jack audio 3.5mm
-   câble audio 3.5mm
-   [adapteur audio stéréo mâle vers 3 broches femelles]
-   [kit de fabrication de moules en silicone]
-   [perles thermoplastiques]
-   [kit de résine époxy transparente]
-   [pâte à modeler]

### Sources consultées

Documentation technique sur le DTMF:

-   https://www.itu.int/rec/T-REC-Q.23-198811-I/fr
-   https://www.etsi.org/deliver/etsi_tr/101000_101099/10104102/01.01.01_60/tr_10104102v010101p.pdf

Protocole WS2812:

-   https://cdn-shop.adafruit.com/datasheets/WS2812.pdf
-   https://blog.adafruit.com/2017/05/03/psa-the-ws2812b-rgb-led-has-been-revised-will-require-code-tweak/

Comment lire l'audio avec un Arduino:

-   https://electronics.stackexchange.com/questions/692732/how-can-i-read-audio-output-signal-jack-with-arduino
-   https://forum.arduino.cc/t/how-to-read-data-from-audio-jack/458301
-   https://forum.arduino.cc/t/using-audio-as-an-input/488499
-   https://www.instructables.com/Arduino-Audio-Input/

Autres:

-   https://www.allaboutcircuits.com/tools/led-resistor-calculator/ (comment
    chaîner des DEL en série)
-   https://www.arduino.cc/reference/fr/ (documentation Arduino)
-   https://developer.mozilla.org/fr/docs/Web/API/Web_Storage_API
-   https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API
-   https://www.techno-science.net/definition/2092.html

### Outils numériques utilisés

-   **[Resistor Calculator]** pour trouver les bonnes résistances
-   **[LED Calculator]** et **[Series LED Resistor Calculator]** pour obtenir
    les valeurs des résistances à utiliser
-   **[Tinkercad]** pour le prototypage
-   **[CircuitLab]** pour la schématisation électrique
-   **[Éditeur web Arduino]** pour écrire et imager le micrologiciel
-   **[Online Tone Generator]** pour tester les signaux DTMF
-   **[Microsoft Teams]** pour la coordination de l'équipe
-   **[Microsoft 365 Word]** et **[Microsoft 365 PowerPoint]** pour produire les
    présentations et rapports écrits

### Bibliothèques expérimentées

-   **[Goertzel library for Arduino]** pour décoder les signaux DTMF
-   **[Adafruit NeoPixel]** pour contrôler les anneaux de lumières
-   **[FastLED]** pour contrôler les anneaux de lumières
-   **[play-dtmf]** pour générer des signaux DTMF dans le navigateur
-   **[TaskScheduler]** pour gérer les tâches en parallèle sur le
    microcontrôleur

### Site statique de documentation

-   **[GitLab]** pour héberger le code source et le site statique
-   **[Docsify]** pour générer le site statique en temps réel
-   **[Gaphor]**, **[GIMP]** et **[Inkscape]** pour les diagrammes explicatifs
-   **[Squoosh]** et **[SVGOMG]** pour optimiser les images du site statique
-   **[Prism]** pour la coloration syntaxique
-   **[Cloudflare]** en tant que proxy inverse et hébergeur DNS

### Application web BarbeGen

-   **[Svelte]** pour structurer l'application web

<!--

Les URLs des liens vont en-dessous de ce bloc:

-->

[Adafruit NeoPixel]: https://github.com/adafruit/Adafruit_NeoPixel
[Arduino Pro Micro]: https://www.pcboard.ca/arduino-pro-micro
[Arduino UNO R3]: https://docs.arduino.cc/hardware/uno-rev3/
[calculateur de résistances]: https://calculator.net/resistor-calculator.html
[CircuitLab]: https://www.circuitlab.com/
[Cloudflare]: https://www.cloudflare.com/
[Docsify]: https://docsify.js.org
[DTMF]: https://www.itu.int/rec/T-REC-Q.23-198811-I/fr
[Éditeur web Arduino]: https://app.arduino.cc
[Gaphor]: https://gaphor.org/
[GIMP]: https://www.gimp.org/
[GitLab]: https://gitlab.com
[Goertzel library for Arduino]: https://github.com/AI5GW/Goertzel
[Inkscape]: https://inkscape.org/
[LED Calculator]: https://ledcalculator.net/
[LocalStorage]: https://developer.mozilla.org/fr/docs/Web/API/Web_Storage_API
[Microsoft 365 PowerPoint]: https://office.com
[Microsoft 365 Word]: https://office.com
[Microsoft Teams]: https://teams.microsoft.com
[Online Tone Generator]: https://onlinetonegenerator.com/dtmf.html
[Prism]: https://prismjs.com/
[Resistor Calculator]: https://calculator.net/resistor-calculator.html
[RS-232]: https://www.weschler.com/blog/about-rs-232-devices/
[Seeed Studio XIAO SAMD21]: https://wiki.seeedstudio.com/Seeeduino-XIAO/
[Series LED Resistor Calculator]: https://kutt.it/V35fsM
[Squoosh]: https://squoosh.app/
[SVGOMG]: https://svgomg.net/
[système hexadécimal]: https://www.techno-science.net/definition/2092.html
[Tinkercad]: https://www.tinkercad.com
[Waveshare RP2040-Zero]: https://www.waveshare.com/wiki/RP2040-Zero
[Web Audio API]: https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API
[WS2812]: https://cdn-shop.adafruit.com/datasheets/WS2812.pdf
[FastLED]: https://github.com/FastLED/FastLED
[play-dtmf]: https://github.com/andsouto/play-dtmf
[Svelte]: https://svelte.dev/
[Git]: https://gitlab.com/barbe.dad
[BarbeCtl]: https://gitlab.com/barbe.dad/barbectl/-/blob/main/src/sketch.ino
[BarbeGen]: https://gitlab.com/barbe.dad/barbegen
[nombreuses]: https://forum.arduino.cc/t/using-audio-as-an-input/488499
[sources]: https://www.instructables.com/Arduino-Audio-Input/
[conflictuelles]: https://forum.arduino.cc/t/h/458301/1
[un fil de discussion]: https://electronics.stackexchange.com/q/692732
[TaskScheduler]: https://github.com/arkhipenko/TaskScheduler

<!-- Produits achetés -->

[fibres optiques]:
    https://www.amazon.ca/-/fr/34CZWGAPSYWY41116/dp/B0CXDGP9MB/ref=sr_1_36?__mk_fr_CA=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=WM3CZJGYFBZ&dib=eyJ2IjoiMSJ9.ytvS-xlBSYNW9gv6Cp67IMXPqI_JUV2TpDVI3yEqle6NNjD3QmF6biDmAvf6tMBKg0DHaWC_EotaTtLGYmNpKf5vI6BN4L0t082g3TMRo1QdP8DtBu2y-o8s06hsY9mRrN2jN7OoixoIwvxEB93mHvhjvgEzI-LP2z_LYbEKbY00s0xhTH4PcDotE5eR2NFwYWVd1Cl9wi0jHfpyeCC6NOUNbLhREe_MdwdI92-j3gQDEmfei89MgHQRqppfdKAvc0MlsV5n7tQIOrJDtCKmJtCqXX0_VirqfpGADSUatdI.RnzTanw4x-ctwF8Ti6kIHiIoC87V63AlX204lGm_vBY&dib_tag=se&keywords=lumiere+led+fibre+optique+guirlande&qid=1712151454&sprefix=lumiere+led+fibre+optique+guirlande%2Caps%2C93&sr=8-36
[kit de fabrication de moules en silicone]:
    https://www.amazon.ca/-/fr/fabrication-silicone-parties-rapport-caoutchouc/dp/B0BZ85G8NJ/ref=sr_1_20?crid=19BZKT9807Y5M&dib=eyJ2IjoiMSJ9.u22IC3tHZrdRh2y8qJVgFwqkqZ0Wi_nNyPeRk_HYFrwjoZGpVbbBfr_XLp7-vRtvXaiMCTXh8woZ8GwXUNOiH7wSrUQeChu7DVOhXEAHnlGWO_kqw8O8ajWAZq1w5VONaxAr6Ck7gZkYrtiyFMlamrYgAo5O9rCT-NBGOl4uDZlXQdD72nx_hChEUq74quA1kQ15AhmkZFMHuU3ePDqg1s7CuHZAd2NrPg9M5rlyldY3TiR2EBXt5S5F1PhOu1C9c-VBKURGUOTCiFMMtwLFYhzKvZ9b1eIFpdAQpE0uslU.d_Gx-EiItRfsjgd3Sl7gSnEK1my-3a5qfWg3p5Mjxmw&dib_tag=se&keywords=silicone+liquide+pour+moulage&qid=1712150926&sprefix=silicone+liqu%2Caps%2C90&sr=8-20
[perles thermoplastiques]:
    https://www.amazon.ca/-/fr/Sukh-Granul%C3%A9s-plastique-thermoplastiques-mall%C3%A9ables/dp/B0C1YX39S5/ref=sr_1_1?crid=3DXWX14TWAKCM&dib=eyJ2IjoiMSJ9.Rn3ExCJQjzWhMoldPyt7FvgObYD8iXOoSt1AQX4blTQOukPWFgLI0_AxyXpm0T-zzPEzefkZ1WPtq1PjR3RbKu1Waomt5J7eo4lF5bKhvVs6hRnpKBmz3uU17A3_6jZLFb90XzQBzJJw450u7FnjkWmIhgIsI-CdAlWY1Q1LG9h9sLBUQBRO8mYRxrRAtu9ndBE5lfWeSysv9AMSdQVmDxjqrVAYBKOa5aXwlTlsShP90_W_w6eR18dhaUzHqgniVrR8Jer77bbS6JwVfogfk5jk8HKLjy7pVd0HxTSKSSg.aXdOgk8PucYUAN1j7lCtY9PqGdqvmKrJkSfRl1IYnzQ&dib_tag=se&keywords=bille+thermoplastique&qid=1712150879&sprefix=bille+the%2Caps%2C85&sr=8-1
[kit de résine époxy transparente]:
    https://www.amazon.ca/-/fr/Kit-r%C3%A9sine-%C3%A9poxy-transparente-fabrication/dp/B092MQN85L/ref=sr_1_34?crid=1HF6J6TWIAR9R&dib=eyJ2IjoiMSJ9.kzkBxvvIjPiVrqunBZx0M2HGISgDyoMX4FflfVVw9ync3mbopu-cNarV1yIgAE4GnPh7sYFMIxh8lH_9bZyYE1gAoVm5EoMhk9mlF602it6b4JHc3mrcXb4u3J7P8P75fgeKGYu_7QEPA9h_Dl7keyjJkG0NlIRBIoq5ziF0ZdwayZr7tvPic6a7mn1Exr5tTfYYxMgesV0V5GTrfuHzAfYU46jVNe57ufkAlLoZnHTR7o1JZWVAjXhbhjUO7PyNUralpOhBjuQvgCc1965Fkeo8T4SUfnkhWdgr9z0-e6c.CLqtSK3NWH2BdY9V4HHCr7DMGp22ciyFGR23IJiXt3M&dib_tag=se&keywords=epoxy+resin&qid=1712151006&sprefix=epox%2Caps%2C98&sr=8-34
[pâte à modeler]:
    https://www.dollarama.com/fr-ca/p-pate-a-modeler-couleurs-assorties/3066573
[adapteur audio stéréo mâle vers 3 broches femelles]:
    https://www.amazon.com/zdyCGTime-Terminal-Headphone-Adapter-Cable%EF%BC%8830CM/dp/B07QNHM52W/ref=sr_1_18?dib=eyJ2IjoiMSJ9.MqZ9aJny4LDKwFIhqK4v4-xviSinroq0pHM8E646XgZxhQIWU9XEZ6z31kHTKPTAgF054hwVZ1pSSxRLXMBkRUhVD84ZwTytRH66BOq6W5zBMEMYlZOQalMZN6GHwup6IGWEhPOmxIRyqz0bpdMj5WxKeKvE-JR1NTMzTKITRHxqdaOPEcIZjKoc2OooxAjgqqMUSjmdcdK53KHGQXT0CYeoCFiPV8fEc_YfhpclpMA.dT7p1-1bk07fgdXhiwyBAnJSEl9z5Xg-B54sGNA8Vzk&dib_tag=se&keywords=zdyCGTime&qid=1712707711&sr=8-18&th=1
[anneaux lumineux]:
    https://www.amazon.ca/-/fr/Stemedu-lumineux-Adressable-individuellement-Raspberry/dp/B0BY8ZP214/ref=sr_1_4?__mk_fr_CA=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3K6V2UEK3HXLS&dib=eyJ2IjoiMSJ9.pUFMt1UGzxMNKgaGWQOKrIG7rjFBgxF5yfal4hYljLSNSTksmXbPPdrAhcR73iNQb4ukZig9y9cZYIO7o-NRU96M7X0SwNsUG-Dyqadj6nPlCLPbOm84o7xyfPHaAxoC_g5uE-3hczAuLn5Ki56teeUhujdQUcFfYHJxIyhFHgq531TyBrUPHEkpKEVJm5ZWmEK9gbXbfSTi13OcmI66V2-VN9ojqlbIWppboDifVuQkKOD4Ca_nnRVRvITzUUkS-OrJYCndgDmSSn68SeK2odh-YJvV0M3kA-1dlFPZ62M.CcF3759OZrAXK5ITPASnhpALjCRgpm8_XbNqgUq-1OY&dib_tag=se&keywords=anneau+led&qid=1712708896&s=industrial&sprefix=anneau+led%2Cindustrial%2C76&sr=1-4
[anneau lumineu]:
    https://www.amazon.ca/-/fr/Stemedu-lumineux-Adressable-individuellement-Raspberry/dp/B0BY8ZP214/ref=sr_1_4?__mk_fr_CA=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3K6V2UEK3HXLS&dib=eyJ2IjoiMSJ9.pUFMt1UGzxMNKgaGWQOKrIG7rjFBgxF5yfal4hYljLSNSTksmXbPPdrAhcR73iNQb4ukZig9y9cZYIO7o-NRU96M7X0SwNsUG-Dyqadj6nPlCLPbOm84o7xyfPHaAxoC_g5uE-3hczAuLn5Ki56teeUhujdQUcFfYHJxIyhFHgq531TyBrUPHEkpKEVJm5ZWmEK9gbXbfSTi13OcmI66V2-VN9ojqlbIWppboDifVuQkKOD4Ca_nnRVRvITzUUkS-OrJYCndgDmSSn68SeK2odh-YJvV0M3kA-1dlFPZ62M.CcF3759OZrAXK5ITPASnhpALjCRgpm8_XbNqgUq-1OY&dib_tag=se&keywords=anneau+led&qid=1712708896&s=industrial&sprefix=anneau+led%2Cindustrial%2C76&sr=1-4
